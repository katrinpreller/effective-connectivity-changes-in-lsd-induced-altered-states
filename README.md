Effective connectivity changes in LSD-induced altered states of consciousness in humans

Preller KH, Razi A, Zeidman P, St�mpfli P, Friston KJ, Vollenweider FX: Effective connectivity changes in LSD-induced altered states of consciousness in humans, PNAS, 2019

For data please see downloads.

Please adress questions to: preller@bli.uzh.ch